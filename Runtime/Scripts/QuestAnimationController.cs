using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    //Enables animation for quest controllers
    [RequireComponent(typeof(Animator))]
    public class QuestAnimationController : MonoBehaviour
    {
        private Animator controllerAnimator = null;

        [SerializeField] private InputActionProperty primaryButton;
        [SerializeField] private InputActionProperty secondaryButton;
        [SerializeField] private InputActionProperty startButton;

        [SerializeField] private InputActionProperty joystick;

        [SerializeField] private InputActionProperty trigger;
        [SerializeField] private InputActionProperty grip;

        private Action<InputAction.CallbackContext> primaryPerformed = null;
        private Action<InputAction.CallbackContext> primaryCanceled = null;

        private Action<InputAction.CallbackContext> secondaryPerformed = null;
        private Action<InputAction.CallbackContext> secondaryCanceled = null;

        private Action<InputAction.CallbackContext> startPerformed = null;
        private Action<InputAction.CallbackContext> startCanceled = null;

        private Action<InputAction.CallbackContext> triggerPerformed = null;
        private Action<InputAction.CallbackContext> triggerCanceled = null;

        private Action<InputAction.CallbackContext> gripPerformed = null;
        private Action<InputAction.CallbackContext> gripCanceled = null;

        private void OnEnable()
        {
            controllerAnimator = GetComponent<Animator>();

            //X
            primaryButton.action.performed += primaryPerformed = (x) => controllerAnimator.SetFloat("Button 1", 1);
            primaryButton.action.canceled += primaryCanceled = (x) => controllerAnimator.SetFloat("Button 1", 0);

            //Y
            secondaryButton.action.performed += secondaryPerformed = (x) => controllerAnimator.SetFloat("Button 2", 1);
            secondaryButton.action.canceled += secondaryCanceled = (x) => controllerAnimator.SetFloat("Button 2", 0);

            //Menu
            startButton.action.performed += startPerformed = (x) => controllerAnimator.SetFloat("Button 3", 1);
            startButton.action.canceled += startCanceled = (x) => controllerAnimator.SetFloat("Button 3", 0);

            //Trigger
            trigger.action.performed += triggerPerformed = (x) => controllerAnimator.SetFloat("Trigger", 1);
            trigger.action.canceled += triggerCanceled = (x) => controllerAnimator.SetFloat("Trigger", 0);

            //Grip
            grip.action.performed += gripPerformed = (x) => controllerAnimator.SetFloat("Grip", 1);
            grip.action.canceled += gripCanceled = (x) => controllerAnimator.SetFloat("Grip", 0);
        }
        private void OnDisable()
        {
            //X
            primaryButton.action.performed -= primaryPerformed;
            primaryButton.action.canceled -= primaryCanceled;

            //Y
            secondaryButton.action.performed -= secondaryPerformed;
            secondaryButton.action.canceled -= secondaryCanceled;

            //Menu
            startButton.action.performed -= startPerformed;
            startButton.action.canceled -= startCanceled;

            //Trigger
            trigger.action.performed -= triggerPerformed;
            trigger.action.canceled -= triggerCanceled;

            //Grip
            grip.action.performed -= gripPerformed;
            grip.action.canceled -= gripCanceled;

            controllerAnimator = null;
        }

        private void Update()
        {
            //Joystick
            Vector2 _joystickMovement = joystick.action.ReadValue<Vector2>();

            controllerAnimator.SetFloat("Joy Y", _joystickMovement.y);
            controllerAnimator.SetFloat("Joy X", _joystickMovement.x);
        }
    }
}